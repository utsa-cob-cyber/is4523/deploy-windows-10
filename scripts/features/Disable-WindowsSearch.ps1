# Disables bing search menu
Write-Host "[+] Disabling Bing Search in Start Menu..." -ForegroundColor Green
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Search" -Name "BingSearchEnabled" -Type DWord -Value 0

# Disables Dynamic Search Box
Write-Host "[+] Disabling Dynamic Search Box in Start Menu..." -ForegroundColor Green
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\SearchSettings" -Name "isDynamicSearchBoxEnabled" -Type DWord -Value 0