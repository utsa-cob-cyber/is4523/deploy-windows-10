# Try to install BoxStarter as is first, then fall back to be over trusing only if this step fails.
Write-Host "[+] Installing BoxStarter...`n" -ForegroundColor Green
try {
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://boxstarter.org/bootstrapper.ps1')); get-boxstarter -Force
    return $true
} catch {
    Write-Host "`t[!] Could not install boxstarter without trust, escalating privileges" -ForegroundColor Yellow
}

# https://stackoverflow.com/questions/11696944/powershell-v3-invoke-webrequest-https-error
# Allows current PowerShell session to trust all certificates
# Also a good find: https://www.briantist.com/errors/could-not-establish-trust-relationship-for-the-ssltls-secure-channel/
try {
    Add-Type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
} catch {
    Write-Host "`t[!] Failed to add new type" -ForegroundColor Yellow
}
try {
    $AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
} catch {
    Write-Host "`t[!] Failed to find SSL type...1" -ForegroundColor Yellow
}
try {
    $AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls'
} catch {
    Write-Host "`t[!] Failed to find SSL type...2" -ForegroundColor Yellow
}
$prevSecProtocol = [System.Net.ServicePointManager]::SecurityProtocol
$prevCertPolicy = [System.Net.ServicePointManager]::CertificatePolicy

# Become overly trusting
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
# download and instal boxstarter
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://boxstarter.org/bootstrapper.ps1')); get-boxstarter -Force
# Restore previous trust settings for this PowerShell session
# Note: SSL certs trusted from installing BoxStarter above will be trusted for the remaining PS session
[System.Net.ServicePointManager]::SecurityProtocol = $prevSecProtocol
[System.Net.ServicePointManager]::CertificatePolicy = $prevCertPolicy
# return $true