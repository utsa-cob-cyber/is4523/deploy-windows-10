# Check if winget is installed
Write-Host "[+] Installing Windows GET...`n" -ForegroundColor Green
if (Test-Path ~\AppData\Local\Microsoft\WindowsApps\winget.exe){
    'Winget Already Installed'
}  
else {
    # Installing winget from the Microsoft Store
	Write-Host "`t[!] Winget not found, installing it now."
    $ResultText.text = "`r`n" +"`r`n" + "Installing Winget... Please Wait"
	Start-Process "ms-appinstaller:?source=https://aka.ms/getwinget"
	$nid = (Get-Process AppInstaller).Id
	Wait-Process -Id $nid
	Write-Host "`t[!] Installed winget."
    $ResultText.text = "`r`n" +"`r`n" + "Winget Installed - Ready for Next Task"
}