# Install OpenSSH server 
Write-Host "[+] Installing OpenSSH Server..." -ForegroundColor Green
Get-WindowsCapability -Online | Where-Object { $_.Name -like "OpenSSH.Server*" } | Add-WindowsCapability -Online | Out-Null

# Uninstall OpenSSH server 
Write-Host "[+] Uninstalling OpenSSH Server..." -ForegroundColor Green
Get-WindowsCapability -Online | Where-Object { $_.Name -like "OpenSSH.Server*" } | Remove-WindowsCapability -Online | Out-Null

# Uninstall OpenSSH client
Write-Host "[+] Uninstalling OpenSSH Client..." -ForegroundColor Green
Get-WindowsCapability -Online | Where-Object { $_.Name -like "OpenSSH.Client*" } | Remove-WindowsCapability -Online | Out-Null

# Install OpenSSH client
Write-Host "[+] Installing OpenSSH Client..." -ForegroundColor Green
Get-WindowsCapability -Online | Where-Object { $_.Name -like "OpenSSH.Client*" } | Add-WindowsCapability -Online | Out-Null