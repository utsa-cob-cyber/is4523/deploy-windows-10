# Disables news and interests on the bottom right of the UI
Write-Host "[+] Disabling News and Interests...`n" -ForegroundColor Green
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Feeds" -Name "ShellFeedsTaskbarViewMode" -Value 2
New-Item -Path "HKLM:\Software\Policies\Microsoft\Windows" -Name "Windows Feeds" -Type "Key" -Force
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\Windows Feeds" -Name "EnableFeeds" -Type "DWord" -Value 0 -Force