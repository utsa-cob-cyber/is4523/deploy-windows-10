# Install third-party software not in chocolatey
$software_url = "https://cc-public.s3.amazonaws.com/AccessData_FTK_Imager_4.5.0_(x64).exe"
$ProgressPreference = "SilentlyContinue"
Invoke-WebRequest -Uri $software_url -OutFile "$env:TEMP\installer.exe"
Write-Host "[+] Installing $software_url"
Start-Process -FilePath "installer.exe" -ArgumentList "/S /vqn" -WorkingDirectory "$env:TEMP"
Remove-Item "$env:TEMP\installer.exe"