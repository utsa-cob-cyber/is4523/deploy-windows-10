# Disable Windows dark mode
Write-Host "[+] Disabling dark mode..." -ForegroundColor Green
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name "AppsUseLightTheme" -Value 1
Write-Host "[!] Dark mode disabled!`n" -ForegroundColor Green

# Enable Windows dark mode
Write-Host "[+] Enabling dark mode..." -ForegroundColor Green
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name "AppsUseLightTheme" -Value 0
Write-Host "[!] Dark mode enabled!`n" -ForegroundColor Green 