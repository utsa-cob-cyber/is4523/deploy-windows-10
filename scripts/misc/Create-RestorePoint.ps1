# Create a windows restore point
Write-Host "[+] Creating a restore point..." -ForegroundColor Green
Enable-ComputerRestore -Drive "C:\"
Checkpoint-Computer -Description "RestorePoint1" -RestorePointType "MODIFY_SETTINGS"