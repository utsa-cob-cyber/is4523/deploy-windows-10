# Check to make sure host is supported
Write-Host "[+] Checking Operating System Compatibility..." -ForegroundColor Green
if (-Not (([System.Environment]::OSVersion.Version.Major -eq 10))) {
    Write-Host "`t[*] $((Get-WmiObject -class Win32_OperatingSystem).Caption) is not supported, please use Windows 7 Service Pack 1 or Windows 10" -ForegroundColor Red
    exit
} 
else {
    Write-Host "`t[!] $((Get-WmiObject -class Win32_OperatingSystem).Caption) supported" -ForegroundColor Green
}