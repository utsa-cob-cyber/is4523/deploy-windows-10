##############################################################################
# Install WSL script
# Purpose: Installs Windows Subsystem for Linux
# Author:  Jacob Stauffer | CISSP
##############################################################################
<#
.SYNOPSIS
    WSL installation Script
.DESCRIPTION
    This script will configure system with all required software for IS4523.
.EXAMPLE
    is4523.ps1
#>

# Install Linux Subsystem - Applicable since Win10 1607 and Server 1709
Write-Host "[+] Installing Linux Subsystem..." -ForegroundColor Green
wsl --install
wsl --shutdown
netsh winsock reset
netsh int ip reset all
netsh winhttp reset proxy
ipconfig /flushdns
netsh winsock reset 
#Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux | Enable-WindowsOptionalFeature -Online -All -NoRestart | Out-Null

# Install the Ubuntu WSL distribution
Write-Host "[+] Installing the Ubuntu WSL distribution..." -ForegroundColor Green
wsl.exe --install -d Ubuntu-20.04